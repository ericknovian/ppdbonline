<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('view_home');
	}

	public function profil ($namaorang ='')
	{
		/*if ($namaorang == 'fuad')
			$this->load->view('view_fuad');
		else if ($namaorang == 'erik')
			$this->load->view('view_erik');
		else if ($namaorang == 'dani')
			$this->load->view('view_dani');
		else if ($namaorang == '')
			$this->load->view('view_profil');
		else 
			$this->load->view('view_notfound');*/


		if ($namaorang == '')
		{
			$this->load->view('view_profil');
		}
		else
		{
			$data = array(
				'nama'=> $namaorang,
				'umur'=> 17
			);

			$this->load->view('show_profil', $data);
		}


	}
	}
	
