<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/*function __construct(){
		parent:: __construct();

		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}
	}*/

	public function index()
	{
		$data = array (
				'judul' => 'Dasboard Utama',
				'view'  => 'view_dasbord' 
			);
			$this->load->view('view_master',$data);
	}


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function login()
	{
		$this->load->view('view_login');

	}
	public function daftar($aksi = 'lihat')
	{
		if($aksi == 'lihat')
		{
			$data = array(
				'judul' => 'Data Berkas',
				'view' => 'siswa/view_daftar'
			);
			$this->load->view('view_master', $data);
		}
		else if($aksi == 'tambah')
		{
			$data = array(
				'judul' => 'Tambah Siswa',
				'view' => 'siswa/tambah_siswa'
			);
			$this->load->view('view_master', $data);
		}
		else if($aksi == 'edit')
		{
			$data = array(
				'judul' => 'Edit Siswa',
				'view' => 'siswa/edit_siswa'
			);
			$this->load->view('view_master', $data);
		}
	}

	public function aksi_tambahsiswa()
	{
		$nisn = $_POST['nisn'];
		$nama = $_POST['nama'];
		$tempatlahir = $_POST['tempatlahir'];
		$jeniskelamin = $_POST['jeniskelamin'];
		$tanggallahir = $_POST['tanggallahir'];
		$kelas = $_POST['kelas'];
		$alamat = $_POST['alamat'];
		$kota = $_POST['kota'];
		
		echo "nisn = $nisn <br/>";
		echo "nama = $nama <br/>";
		echo "tempatlahir = $tempatlahir <br/>";
		echo "tanggallahir = $tanggallahir <br/>";
		echo "jeniskelamin = $jeniskelamin <br/>";
		echo "kelas = $kelas <br/>";
		echo "alamat = $alamat <br/>";
		echo "kota = $kota <br/>";

		$tgl = explode("/", $tanggallahir);
		$hari = $tgl[1];
		$bulan = $tgl[0];
		$tahun = $tgl[2];

		$tgl_jadi = $tahun . "-" . $bulan . "-" . $hari;

		echo "<b>$tgl_jadi</b>"; 

		$this->load->database();

		$query = "INSERT INTO `tbl_siswa` (`nisn`, `nama`, `tempatlahir`, `tangallahir`, `jeniskelamin`, `kelas`, `alamat`, `kota`) VALUES ('$nisn', '$nama', '$tempatlahir', '$tgl_jadi', '$jeniskelamin', '$kelas', '$alamat', '$kota')";

		$hasil = $this->db->query($query);
		if($hasil == true)
		{
			//echo "Data berhasil disimpan";
			redirect('admin/siswa/lihat');
		}
		else
		{
			echo "Data Gagal disimpan";
		}

	}

	public function siswa($aksi = 'lihat')
	{
		if($aksi == 'lihat')
		{

			$this->load->database();

			$q = "SELECT * FROM tbl_siswa";
			$siswa = $this->db->query($q)->result();

			$data = array(
				'judul' => 'Data Siswa',
				'view' => 'siswa/lihat_siswa',
				'siswa' => $siswa
			);
			$this->load->view('view_master', $data);
		}
		else if($aksi == 'tambah')
		{
			$data = array(
				'judul' => 'Tambah Siswa',
				'view' => 'siswa/tambah_siswa'
			);
			$this->load->view('view_master', $data);
		}
		else if($aksi == 'edit')
		{
			$data = array(
				'judul' => 'Edit Siswa',
				'view' => 'siswa/edit_siswa'
			);
			$this->load->view('view_master', $data);
		}
	}
	public function guru($aksi = 'lihat')
	{
		if($aksi == 'lihat')
		{
			$data = array(
				'judul' => 'Data Guru',
				'view' => 'guru/lihat_guru'
			);
			$this->load->view('view_master', $data);
		}
		else if($aksi == 'tambah')
		{
			$data = array(
				'judul' => 'Tambah Guru',
				'view' => 'guru/tambah_guru'
			);
			$this->load->view('view_master', $data);
		}
		else if($aksi == 'edit')
		{
			$data = array(
				'judul' => 'Edit Guru',
				'view' => 'guru/edit_guru'
			);
			$this->load->view('view_master', $data);
		}
	}
}