<div class='row'>
	<div class="col-md-12">
		<div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Masukan data guru dengan benar</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputnis">NIP</label>
                      <input type="number" class="form-control" id="Input NISN" placeholder="Masukan NISN">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Nama</label>
                      <input type="password" class="form-control" id="exampleInputname" placeholder="Tulis Nama">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tanggal Lahir</label>
                      <input type="password" class="form-control" id="exampleInputname" placeholder="Tulis Tanggal Lahir Siswa">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">File input</label>
                      <input type="file" id="exampleInputFile">
                      <p class="help-block">Example block-level help text here.</p>
                    </div>
                    <div class="checkbox">
                      <label>
                        <input type="checkbox"> Check me out
                      </label>
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
                </form>
              </div><!-- /.box -->
	</div>
</div>