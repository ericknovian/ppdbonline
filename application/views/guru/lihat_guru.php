<div class='row'>
	<div class="col-md-12">
		<div class="box">
                <div class="box-header">
                  <div class="box-tools">
                    <div class="input-group">
                      <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                	<a href=<?php echo base_url(); ?>admin/guru/tambah class="btn btn-primary">Tambah Guru</a> 
                  <a href=<?php echo base_url(); ?>admin/guru/edit class="btn btn-primary">Edit Guru</a> 
                  <table class="table table-hover">
                    <tr>
                      <th>No</th>
                      <th>No. Daftar</th>
                      <th>Nama Calon Siswa</th>
                      <th>Jenis Kelamin</th>
                      <th>Tahun Lulus</th>
                    </tr>
                    <tr>
                      <td>001</td>
                      <td>160441100024</td>
                      <td>Suryo Dwiryo</td>
                      <td><span class="label label-success">Laki-laki</span></td>
                      <td>2017</td>
                    </tr>
                    <tr>
                      <td>002</td>
                      <td>160441100025</td>
                      <td>Agustina Rahayu</td>
                      <td><span class="label label-warning">Perempuan</span></td>
                      <td>2019</td>
                    </tr>
                    <tr>
                      <td>003</td>
                      <td>160441100026</td>
                      <td>Arya Wiguna</td>
                      <td><span class="label label-success">Laki-laki</span></td>
                      <td>2020</td>
                    </tr>
                    <tr>
                      <td>004</td>
                      <td>160441100027</td>
                      <td>Anang Hermansyah</td>
                      <td><span class="label label-success">Laki-laki</span></td>
                      <td>2017</td>
                    </tr>
                    <tr>
                      <td>005</td>
                      <td>160441100029</td>
                      <td>Astutik</td>
                      <td><span class="label label-warning">Perempuan</span></td>
                      <td>2015</td>
                    </tr>
                    <tr>
                      <td>006</td>
                      <td>160441100031</td>
                      <td>Neneng</td>
                      <td><span class="label label-warning">Perempuan</span></td>
                      <td>2019</td>
                    </tr>
                    <tr>
                      <td>007</td>
                      <td>160441100032</td>
                      <td>Indah Kunani</td>
                      <td><span class="label label-warning">Perempuan</span></td>
                      <td>2014</td>
                    </tr>
                    <tr>
                      <td>008</td>
                      <td>160441100033</td>
                      <td>Agra nigra</td>
                      <td><span class="label label-warning">Perempuan</span></td>
                      <td>2017</td>
                    </tr>
                  </table>
                </div><!-- /.box-body -->
              
	</div>
</div>