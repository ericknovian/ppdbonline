<div class='row'>
  <div class="col-md-12">
    <div class="box">
                <div class="box-header">
                  <div class="box-tools">
                    <div class="input-group">
                      <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <a href=<?php echo base_url(); ?>admin/siswa/tambah class="btn btn-primary">Tambah Siswa</a> 
                  <a href=<?php echo base_url(); ?>admin/siswa/edit class="btn btn-primary">Edit Siswa</a> 
                  <table class="table table-hover">
                    <tr>
                      <th>NiSN</th>
                      <th>Nama Siswa</th>
                      <th>TTL</th>
                      <th>Jenis Kelamin</th>
                      <th>Kelas</th>
                      <th>Kota</th>
                    </tr>

                    <?php
                    foreach ($siswa as $s) {
                          $kelamin = '';
                          if($s->jeniskelamin == 'L')
                            $jeniskelamin = 'Laki-laki';
                          else
                            $jeniskelamin = 'Perempuan';

                          echo "
                          <tr>
                            <td>$s->nisn</td>
                            <td>$s->nama</td>
                            <td>$s->tempatlahir, $s->tanggallahir</td>
                            <td><span class='label label-success'>$s->jeniskelamin</span></td>
                            <td>$s->kelas</td>
                            <td>$s->kota</td>
                          </tr>";
                    }
                    ?>
                  </table>
                </div><!-- /.box-body -->
              
  </div>
</div>