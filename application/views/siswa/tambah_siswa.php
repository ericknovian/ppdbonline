<div class='row'>
	<div class="col-md-12">
		<div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Masukan data siswa dengan benar</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" action="<?php echo base_url(); ?>admin/aksi_tambahsiswa">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputnis">NSN</label>
                      <input type="number" name="nisn" class="form-control" id="Inputnisn" placeholder="Masukan NISN">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Nama Siswa</label>
                      <input type="text" name="nama" class="form-control" id="exampleInputname" placeholder="Tulis Nama">
                    </div>
                    <div class="form-group">
                      <label for="inputtempatlahir">Tempat Lahir</label>
                      <input type="text" name="tempatlahir" class="form-control" id="inputtempatlahir" placeholder="Tulis Tanggal Lahir Siswa">
                    </div>
                    <div class="form-group">
                    <label>Tanggal Lahir</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="tanggallahir" class="form-control pull-right" id="tanggallahir"/>
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <!-- radio -->
                  <div class="form-group">
                      <label for="inputtempatlahir">Jenis Kelamin</label>
                      <div class="radio">
                        <label>
                          <input type="radio" name="jeniskelamin" id="optionslaki" value="L" checked>
                          Laki-laki
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="jeniskelamin" id="optionsperempuan" value="P">
                          Perempuan
                        </label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Kelas</label>
                      <select class="form-control" name="kelas">
                        <option>Kelas 1</option>
                        <option>Kelas 2</option>
                        <option>Kelas 3</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>Alamat</label>
                      <textarea class="form-control" name="alamat" rows="4" placeholder="Masukan Alamat Rumah...."></textarea>
                    </div>
                    <div class="form-group">
                      <label>Kota</label>
                      <select class="form-control" name="kota">
                        <option>Bangkalan</option>
                        <option>SUrabaya</option>
                        <option>Pamekasan</option>
                        <option>Bangkalan</option>
                        <option>SUrabaya</option>
                        <option>Pamekasan</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">File input</label>
                      <input type="file" id="exampleInputFile">
                      <p class="help-block">Example block-level help text here.</p>
                    </div>
                    <div class="checkbox">
                      <label>
                        <input type="checkbox"> Check me out
                      </label>
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
                </form>
              </div><!-- /.box -->
	</div>
</div>